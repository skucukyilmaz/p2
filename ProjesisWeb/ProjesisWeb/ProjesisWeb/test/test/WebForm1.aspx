﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="ProjesisWeb.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 
    <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" id="FirmsAddButton" class="btn btn-secondary">Ekle</button>
            <button type="button" id="FirmsDeleteButton" class="btn btn-secondary">Sil</button>
            <button type="button" id="FirmsEditButton" class="btn btn-secondary">Düzelt</button>
            <button type="button" id="FirmsSaveButton" class="btn btn-secondary">Kaydet</button>
            <button type="button" id="FirmsCancelButton" class="btn btn-secondary">Vazgeç</button>
        </div>
        <hr style="border: groove" />
        <b style="margin-left: 10px;">Detay</b>
        <hr style="border: groove" />

        <%--<div class="input-group" style="float: left; margin-left: 10px">
            <div class="input-group-prepend">
                <span class="input-group-text" style="margin-right: 150px" id="firmCodeLabel">Kod</span>
                <input type="text" class="form-control" placeholder="Kod giriniz" aria-describedby="basic-addon1" />&nbsp;&nbsp;
                    <br />
                <span class="input-group-text" style="margin-right: 158px" id="firmNameLabel">Ad</span>
                <input type="text" class="form-control" placeholder="Firma adı giriniz" aria-label="Username" aria-describedby="basic-addon1" />
                <br />
                <span class="input-group-text" style="margin-right: 10px" id="firmAvailTargetLabel">Kullanılabilirlik Hedefi %</span>
                <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />
                <br />
                <span class="input-group-text" style="margin-right: 74px" id="firmQualityTargetLabel">Kalite Hedefi %</span>
                <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />

                <span class="input-group-text" style="margin-right: 10px" id="firmOEETargetLabel">OEE Hedefi %</span>
                <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />

                <span class="input-group-text" style="margin-right: 46px" id="firmSetupTargetLabel">Setup Hedefi/Değeri</span>
                <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                <hr />
            </div>
        </div>--%>

        <%--<div class="input-group" style="float: none; margin-left: 10px">
            <div class="input-group">
                <span class="input-group-text" style="margin-right: 10px" id="firmPerfTimeLabel">Performans Ölçüm Süresi(dk)</span>
                <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                <br />
                <span class="input-group-text" style="margin-right: 10px" id="firmAvailTimeLabel">Kullanılabilirlik Ölçüm Süresi(dk)</span>
                <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
        </div>--%>


        <%--        <div class="form-row">
            <div class="form-group col-sm-12">

                <span class="input-group-text" style="margin-right: 150px" id="firmCodeLabel">Kod</span>
                <input type="text" class="form-control" placeholder="Kod giriniz" aria-describedby="basic-addon1" />
            </div>

            <div class="form-group col-sm-5">
                <span class="input-group-text" style="margin-right: 10px" id="firmPerfTimeLabel">Performans Ölçüm Süresi(dk)</span>
                <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
        </div>
        <%-- <span class="input-group-text" style="margin-right: 10px" id="firmAvailTimeLabel">Kullanılabilirlik Ölçüm Süresi(dk)</span>
                <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />--%>

        <%--
        <div class="form-group">
            <span class="input-group-text" style="margin-right: 158px" id="firmNameLabel">Ad</span>
            <input type="text" class="form-control" placeholder="Firma adı giriniz" aria-label="Username" aria-describedby="basic-addon1" />
        </div>

        <div class="form-group">
            <span class="input-group-text" style="margin-right: 10px" id="firmAvailTargetLabel">Kullanılabilirlik Hedefi %</span>
            <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />
        </div>

        <div class="form-group">
            <span class="input-group-text" style="margin-right: 74px" id="firmPermTargetLabel">Performans Hedefi %</span>
            <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />
        </div>
        <div class="form-row">
            <div class="form-group col-sm-6">
                <span class="input-group-text" style="margin-right: 74px" id="firmQualityTargetLabel">Kalite Hedefi %</span>
                <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
            <div class="form-group col-sm-6">
                <span class="input-group-text" style="margin-right: 10px" id="firmOEETargetLabel">OEE Hedefi %</span>
                <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
            </div>
        </div>

        <div class="form-group">
            <span class="input-group-text" style="margin-right: 46px" id="firmSetupTargetLabel">Setup Hedefi/Değeri</span>
            <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
        </div>--%>


        <div class="container">
            <div class="row">
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmCodeLabel">Kod</span>
                    <input type="text" class="form-control" placeholder="Kod giriniz" aria-describedby="basic-addon1" />
                </div>
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmPerfTimeLabel">Performans Ölçüm Süresi(dk)</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmNameLabel">Ad</span>
                    <input type="text" class="form-control" placeholder="Firma adı giriniz" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmAvailTimeLabel">Kullanılabilirlik Ölçüm Süresi(dk)</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmAvailTargetLabel">Kullanılabilirlik Hedefi %</span>
                    <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmFirstWaitTimeLabel">İlk bekleme Süresi</span>
                    <input type="text" class="form-control" placeholder="sn" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmPermTargetLabel">Performans Hedefi %</span>
                    <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmUnCertainStandLabel">Belirsiz Duruşa Geçme Süresi</span>
                    <input type="text" class="form-control" placeholder="sn" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
            </div>

            <div class="row">

                <div class="col-sm-3 form-group">
                    <span class="input-group-text" id="firmQualityTargetLabel">Kalite Hedefi %</span>
                    <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="col-sm-3 form-group">
                    <span class="input-group-text" id="firmOEETargetLabel">OEE Hedefi %</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="col-sm-3 form-group">
                    <span class="input-group-text" id="firmPoliConstLabel">Polivalans Kısıtı Uygulanacak mı?</span>
                    <input type="checkbox" aria-label="Checkbox for following text input">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 form-group">
                    <span class="input-group-text" id="firmSetupTargetLabel">Setup Hedefi/Değeri</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 form-group">
                    <asp:GridView ID="FirmsGridView" runat="server" AutoGenerateColumns="true"
                        DataKeyNames="ID" PageSize="3" AllowPaging="true"
                        EmptyDataText="No records has been added.">
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <%--<Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True"
                                SortExpression="ID" />
                            <asp:TemplateField HeaderText="CODE" SortExpression="CODE">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CODE") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("CODE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NAME" SortExpression="NAME">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("NAME") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>--%>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <%--<div class="container">
            <div class="row">
                <div class="col-sm-4">

                    <span class="input-group-text" style="margin-right: 150px" id="firmCodeLabel">Kod</span>
                    <input type="text" class="form-control" placeholder="Kod giriniz" aria-describedby="basic-addon1" />

                    <span class="input-group-text" style="margin-right: 158px" id="firmNameLabel">Ad</span>
                    <input type="text" class="form-control" placeholder="Firma adı giriniz" aria-label="Username" aria-describedby="basic-addon1" />

                    <span class="input-group-text" style="margin-right: 10px" id="firmAvailTargetLabel">Kullanılabilirlik Hedefi %</span>
                    <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />

                    <span class="input-group-text" style="margin-right: 74px" id="firmQualityTargetLabel">Kalite Hedefi %</span>
                    <input type="text" class="form-control" placeholder="%" aria-label="Username" aria-describedby="basic-addon1" />

                    <span class="input-group-text" style="margin-right: 46px" id="firmSetupTargetLabel">Setup Hedefi/Değeri</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />



                </div>
                <div class="col-sm-4">
                    <span class="input-group-text" style="margin-right: 10px" id="firmOEETargetLabel">OEE Hedefi %</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                </div>

                <div class="col-sm-4">
                    <span class="input-group-text" style="margin-right: 10px" id="firmPerfTimeLabel">Performans Ölçüm Süresi(dk)</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                    <span class="input-group-text" style="margin-right: 10px" id="firmAvailTimeLabel">Kullanılabilirlik Ölçüm Süresi(dk)</span>
                    <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
            </div>
        </div>--%>
</asp:Content>
