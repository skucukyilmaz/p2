﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;


namespace ProjesisWeb.DB
{
    public class ConnectionDB : IDisposable
    {
        public SqlConnection conn;
        public ConnectionDB()
        {
            conn = new SqlConnection("Data Source=LAPTOP-PTLSNAPR;Initial Catalog=PMIDBW;User ID=sa;Password=1234");
            conn.Open();
        }
               

        public void Dispose()
        {
            if (conn.State == System.Data.ConnectionState.Open)
                conn.Close();
            conn.Dispose();
        }
    }

}