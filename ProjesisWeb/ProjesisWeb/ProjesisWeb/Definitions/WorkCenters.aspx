﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkCenters.aspx.cs" Inherits="ProjesisWeb.Definitions.WorkCenters" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">

        <hr style="border: groove" />
        <div class="row">
            <div class="col-sm-12 form-group">
                <dx:ASPxGridView ID="WorkCentersGridView" runat="server" AutoGenerateColumns="False"
                    DataSourceID="SqlDataSource3" KeyFieldName="ID" Width="100%"
                    PageSize="10" AllowPaging="true" Theme="DevEx">

                    <Settings VerticalScrollableHeight="300" ShowFilterRow="True" ShowGroupPanel="True" ShowFooter="True" />
                    <SettingsAdaptivity>
                        <AdaptiveDetailLayoutProperties ColCount="1"></AdaptiveDetailLayoutProperties>
                    </SettingsAdaptivity>

                    <SettingsPager PageSize="5">
                        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                    </SettingsPager>

                    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                    <SettingsBehavior AllowFocusedRow="True" ConfirmDelete="True" />
                    <SettingsSearchPanel Visible="True" />
                    <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="WYSIWYG" />


                    <Columns>
                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="ID" VisibleIndex="1" ReadOnly="True" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CODE" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="NAME" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="POLYVALENCE" VisibleIndex="5">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="QUALITY" VisibleIndex="6">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="DOWNTIME" VisibleIndex="7">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFORMANCE" VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SETUP" VisibleIndex="9">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="ACTIVE" VisibleIndex="10">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFMEASTIME" VisibleIndex="11">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ERRORTIME" VisibleIndex="12">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="WAITTIME" VisibleIndex="13">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="AVAILMEASTIME" VisibleIndex="14">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="COST" VisibleIndex="15">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="VIRTUAL" VisibleIndex="16">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="WODC" VisibleIndex="17">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="BACKTIME" VisibleIndex="18">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="FORWARDTIME" VisibleIndex="19">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="MESSAGE" VisibleIndex="20">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="MESSAGENOTE" VisibleIndex="21">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SKIPDAY" VisibleIndex="22">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="TERMINATEDOWNTIME" VisibleIndex="23">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="WORKCENTEREMPLOYEES" VisibleIndex="24">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="OPERATIONSIGNAL" VisibleIndex="25">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="TOOLS" VisibleIndex="26">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="PLANNEDWORKORDER" VisibleIndex="27">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="SPACE1" VisibleIndex="28">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="RME" VisibleIndex="29">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="MAXQNT" VisibleIndex="30">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OEE" VisibleIndex="31">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="TOOLSCNT" VisibleIndex="32">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="WORKTIME" VisibleIndex="33">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="OEEINC" VisibleIndex="34">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="EMPLOYEESCONTROL" VisibleIndex="35">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="WORKGROUPSCONTROL" VisibleIndex="36">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="PRINTBARCODE" VisibleIndex="37">
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="WCFREEQUANTITY" VisibleIndex="38">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="WCWTYPE" VisibleIndex="39">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="COMPLATEDQCONTROL" VisibleIndex="40">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="FCACTIVE" VisibleIndex="41">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="WODCTYPE" VisibleIndex="42">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="MAINWORKCENTER" VisibleIndex="43">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="MAINWORKCENTERCODE" VisibleIndex="44">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="WORKCENTERTOOLS" VisibleIndex="45">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataComboBoxColumn FieldName="WORKGROUPID" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="GroupsInWorkCenters" TextField="CODE_NAME" ValueField="ID">
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                    </Columns>
                    <SettingsCommandButton>
                        <NewButton Text="Ekle"
                            RenderMode="Button"
                            Styles-Style-BackColor="Green">
                        </NewButton>
                        <UpdateButton Text="Güncelle"
                            RenderMode="Button">
                        </UpdateButton>
                        <CancelButton Text="Vazgeç"
                            RenderMode="Button">
                        </CancelButton>
                        <EditButton Text="Düzenle"
                            RenderMode="Button">
                        </EditButton>
                        <DeleteButton Text="Sil"
                            RenderMode="Button">
                        </DeleteButton>
                        <ClearFilterButton Text="Temizle">
                        </ClearFilterButton>
                    </SettingsCommandButton>

                    <SettingsEditing EditFormColumnCount="3" Mode="PopupEditForm" />
                    <EditFormLayoutProperties ShowItemCaptionColon="False">
                        <SettingsAdaptivity AdaptivityMode="Off" />
                    </EditFormLayoutProperties>
                    <SettingsPopup CustomizationWindow-HorizontalAlign="WindowCenter" EditForm-HorizontalAlign="WindowCenter" EditForm-VerticalAlign="WindowCenter" EditForm-Width="1000">
                        <EditForm>
                            <SettingsAdaptivity Mode="OnWindowInnerWidth" SwitchAtWindowInnerWidth="768" />
                        </EditForm>
                        <CustomizationWindow HorizontalAlign="WindowCenter"></CustomizationWindow>
                    </SettingsPopup>
                    <Toolbars>
                        <dx:GridViewToolbar EnableAdaptivity="true">
                            <Items>
                                <dx:GridViewToolbarItem Text="Excel Aktar" Command="ExportToXlsx" />
                            </Items>
                        </dx:GridViewToolbar>
                    </Toolbars>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                    ConnectionString="<%$ ConnectionStrings:PMIDBWConnectionString2 %>"
                    SelectCommand="SELECT * FROM [WORKCENTERS]" UpdateCommand="UPDATE [WORKCENTERS] SET [WORKGROUPID] = @WORKGROUPID, [CODE] = @CODE, [NAME] = @NAME, [POLYVALENCE] = @POLYVALENCE, [QUALITY] = @QUALITY, [DOWNTIME] = @DOWNTIME, [PERFORMANCE] = @PERFORMANCE, [SETUP] = @SETUP, [ACTIVE] = @ACTIVE, [PERFMEASTIME] = @PERFMEASTIME, [ERRORTIME] = @ERRORTIME, [WAITTIME] = @WAITTIME, [AVAILMEASTIME] = @AVAILMEASTIME, [COST] = @COST, [VIRTUAL] = @VIRTUAL, [WODC] = @WODC, [BACKTIME] = @BACKTIME, [FORWARDTIME] = @FORWARDTIME, [MESSAGE] = @MESSAGE, [MESSAGENOTE] = @MESSAGENOTE, [SKIPDAY] = @SKIPDAY, [TERMINATEDOWNTIME] = @TERMINATEDOWNTIME, [WORKCENTEREMPLOYEES] = @WORKCENTEREMPLOYEES, [OPERATIONSIGNAL] = @OPERATIONSIGNAL, [TOOLS] = @TOOLS, [PLANNEDWORKORDER] = @PLANNEDWORKORDER, [SPACE1] = @SPACE1, [RME] = @RME, [MAXQNT] = @MAXQNT, [OEE] = @OEE, [TOOLSCNT] = @TOOLSCNT, [WORKTIME] = @WORKTIME, [OEEINC] = @OEEINC, [EMPLOYEESCONTROL] = @EMPLOYEESCONTROL, [WORKGROUPSCONTROL] = @WORKGROUPSCONTROL, [PRINTBARCODE] = @PRINTBARCODE, [WCFREEQUANTITY] = @WCFREEQUANTITY, [WCWTYPE] = @WCWTYPE, [COMPLATEDQCONTROL] = @COMPLATEDQCONTROL, [FCACTIVE] = @FCACTIVE, [WODCTYPE] = @WODCTYPE, [MAINWORKCENTER] = @MAINWORKCENTER, [MAINWORKCENTERCODE] = @MAINWORKCENTERCODE, [WORKCENTERTOOLS] = @WORKCENTERTOOLS WHERE [ID] = @original_ID AND [WORKGROUPID] = @original_WORKGROUPID AND [CODE] = @original_CODE AND (([NAME] = @original_NAME) OR ([NAME] IS NULL AND @original_NAME IS NULL)) AND (([POLYVALENCE] = @original_POLYVALENCE) OR ([POLYVALENCE] IS NULL AND @original_POLYVALENCE IS NULL)) AND (([QUALITY] = @original_QUALITY) OR ([QUALITY] IS NULL AND @original_QUALITY IS NULL)) AND (([DOWNTIME] = @original_DOWNTIME) OR ([DOWNTIME] IS NULL AND @original_DOWNTIME IS NULL)) AND (([PERFORMANCE] = @original_PERFORMANCE) OR ([PERFORMANCE] IS NULL AND @original_PERFORMANCE IS NULL)) AND (([SETUP] = @original_SETUP) OR ([SETUP] IS NULL AND @original_SETUP IS NULL)) AND (([ACTIVE] = @original_ACTIVE) OR ([ACTIVE] IS NULL AND @original_ACTIVE IS NULL)) AND (([PERFMEASTIME] = @original_PERFMEASTIME) OR ([PERFMEASTIME] IS NULL AND @original_PERFMEASTIME IS NULL)) AND (([ERRORTIME] = @original_ERRORTIME) OR ([ERRORTIME] IS NULL AND @original_ERRORTIME IS NULL)) AND (([WAITTIME] = @original_WAITTIME) OR ([WAITTIME] IS NULL AND @original_WAITTIME IS NULL)) AND (([AVAILMEASTIME] = @original_AVAILMEASTIME) OR ([AVAILMEASTIME] IS NULL AND @original_AVAILMEASTIME IS NULL)) AND (([COST] = @original_COST) OR ([COST] IS NULL AND @original_COST IS NULL)) AND (([VIRTUAL] = @original_VIRTUAL) OR ([VIRTUAL] IS NULL AND @original_VIRTUAL IS NULL)) AND (([WODC] = @original_WODC) OR ([WODC] IS NULL AND @original_WODC IS NULL)) AND (([BACKTIME] = @original_BACKTIME) OR ([BACKTIME] IS NULL AND @original_BACKTIME IS NULL)) AND (([FORWARDTIME] = @original_FORWARDTIME) OR ([FORWARDTIME] IS NULL AND @original_FORWARDTIME IS NULL)) AND (([MESSAGE] = @original_MESSAGE) OR ([MESSAGE] IS NULL AND @original_MESSAGE IS NULL)) AND (([MESSAGENOTE] = @original_MESSAGENOTE) OR ([MESSAGENOTE] IS NULL AND @original_MESSAGENOTE IS NULL)) AND (([SKIPDAY] = @original_SKIPDAY) OR ([SKIPDAY] IS NULL AND @original_SKIPDAY IS NULL)) AND (([TERMINATEDOWNTIME] = @original_TERMINATEDOWNTIME) OR ([TERMINATEDOWNTIME] IS NULL AND @original_TERMINATEDOWNTIME IS NULL)) AND (([WORKCENTEREMPLOYEES] = @original_WORKCENTEREMPLOYEES) OR ([WORKCENTEREMPLOYEES] IS NULL AND @original_WORKCENTEREMPLOYEES IS NULL)) AND (([OPERATIONSIGNAL] = @original_OPERATIONSIGNAL) OR ([OPERATIONSIGNAL] IS NULL AND @original_OPERATIONSIGNAL IS NULL)) AND (([TOOLS] = @original_TOOLS) OR ([TOOLS] IS NULL AND @original_TOOLS IS NULL)) AND (([PLANNEDWORKORDER] = @original_PLANNEDWORKORDER) OR ([PLANNEDWORKORDER] IS NULL AND @original_PLANNEDWORKORDER IS NULL)) AND (([SPACE1] = @original_SPACE1) OR ([SPACE1] IS NULL AND @original_SPACE1 IS NULL)) AND (([RME] = @original_RME) OR ([RME] IS NULL AND @original_RME IS NULL)) AND (([MAXQNT] = @original_MAXQNT) OR ([MAXQNT] IS NULL AND @original_MAXQNT IS NULL)) AND (([OEE] = @original_OEE) OR ([OEE] IS NULL AND @original_OEE IS NULL)) AND (([TOOLSCNT] = @original_TOOLSCNT) OR ([TOOLSCNT] IS NULL AND @original_TOOLSCNT IS NULL)) AND (([WORKTIME] = @original_WORKTIME) OR ([WORKTIME] IS NULL AND @original_WORKTIME IS NULL)) AND (([OEEINC] = @original_OEEINC) OR ([OEEINC] IS NULL AND @original_OEEINC IS NULL)) AND (([EMPLOYEESCONTROL] = @original_EMPLOYEESCONTROL) OR ([EMPLOYEESCONTROL] IS NULL AND @original_EMPLOYEESCONTROL IS NULL)) AND (([WORKGROUPSCONTROL] = @original_WORKGROUPSCONTROL) OR ([WORKGROUPSCONTROL] IS NULL AND @original_WORKGROUPSCONTROL IS NULL)) AND (([PRINTBARCODE] = @original_PRINTBARCODE) OR ([PRINTBARCODE] IS NULL AND @original_PRINTBARCODE IS NULL)) AND (([WCFREEQUANTITY] = @original_WCFREEQUANTITY) OR ([WCFREEQUANTITY] IS NULL AND @original_WCFREEQUANTITY IS NULL)) AND (([WCWTYPE] = @original_WCWTYPE) OR ([WCWTYPE] IS NULL AND @original_WCWTYPE IS NULL)) AND (([COMPLATEDQCONTROL] = @original_COMPLATEDQCONTROL) OR ([COMPLATEDQCONTROL] IS NULL AND @original_COMPLATEDQCONTROL IS NULL)) AND (([FCACTIVE] = @original_FCACTIVE) OR ([FCACTIVE] IS NULL AND @original_FCACTIVE IS NULL)) AND (([WODCTYPE] = @original_WODCTYPE) OR ([WODCTYPE] IS NULL AND @original_WODCTYPE IS NULL)) AND (([MAINWORKCENTER] = @original_MAINWORKCENTER) OR ([MAINWORKCENTER] IS NULL AND @original_MAINWORKCENTER IS NULL)) AND (([MAINWORKCENTERCODE] = @original_MAINWORKCENTERCODE) OR ([MAINWORKCENTERCODE] IS NULL AND @original_MAINWORKCENTERCODE IS NULL)) AND (([WORKCENTERTOOLS] = @original_WORKCENTERTOOLS) OR ([WORKCENTERTOOLS] IS NULL AND @original_WORKCENTERTOOLS IS NULL))" DeleteCommand="DELETE FROM [WORKCENTERS] WHERE [ID] = @original_ID AND [WORKGROUPID] = @original_WORKGROUPID AND [CODE] = @original_CODE AND (([NAME] = @original_NAME) OR ([NAME] IS NULL AND @original_NAME IS NULL)) AND (([POLYVALENCE] = @original_POLYVALENCE) OR ([POLYVALENCE] IS NULL AND @original_POLYVALENCE IS NULL)) AND (([QUALITY] = @original_QUALITY) OR ([QUALITY] IS NULL AND @original_QUALITY IS NULL)) AND (([DOWNTIME] = @original_DOWNTIME) OR ([DOWNTIME] IS NULL AND @original_DOWNTIME IS NULL)) AND (([PERFORMANCE] = @original_PERFORMANCE) OR ([PERFORMANCE] IS NULL AND @original_PERFORMANCE IS NULL)) AND (([SETUP] = @original_SETUP) OR ([SETUP] IS NULL AND @original_SETUP IS NULL)) AND (([ACTIVE] = @original_ACTIVE) OR ([ACTIVE] IS NULL AND @original_ACTIVE IS NULL)) AND (([PERFMEASTIME] = @original_PERFMEASTIME) OR ([PERFMEASTIME] IS NULL AND @original_PERFMEASTIME IS NULL)) AND (([ERRORTIME] = @original_ERRORTIME) OR ([ERRORTIME] IS NULL AND @original_ERRORTIME IS NULL)) AND (([WAITTIME] = @original_WAITTIME) OR ([WAITTIME] IS NULL AND @original_WAITTIME IS NULL)) AND (([AVAILMEASTIME] = @original_AVAILMEASTIME) OR ([AVAILMEASTIME] IS NULL AND @original_AVAILMEASTIME IS NULL)) AND (([COST] = @original_COST) OR ([COST] IS NULL AND @original_COST IS NULL)) AND (([VIRTUAL] = @original_VIRTUAL) OR ([VIRTUAL] IS NULL AND @original_VIRTUAL IS NULL)) AND (([WODC] = @original_WODC) OR ([WODC] IS NULL AND @original_WODC IS NULL)) AND (([BACKTIME] = @original_BACKTIME) OR ([BACKTIME] IS NULL AND @original_BACKTIME IS NULL)) AND (([FORWARDTIME] = @original_FORWARDTIME) OR ([FORWARDTIME] IS NULL AND @original_FORWARDTIME IS NULL)) AND (([MESSAGE] = @original_MESSAGE) OR ([MESSAGE] IS NULL AND @original_MESSAGE IS NULL)) AND (([MESSAGENOTE] = @original_MESSAGENOTE) OR ([MESSAGENOTE] IS NULL AND @original_MESSAGENOTE IS NULL)) AND (([SKIPDAY] = @original_SKIPDAY) OR ([SKIPDAY] IS NULL AND @original_SKIPDAY IS NULL)) AND (([TERMINATEDOWNTIME] = @original_TERMINATEDOWNTIME) OR ([TERMINATEDOWNTIME] IS NULL AND @original_TERMINATEDOWNTIME IS NULL)) AND (([WORKCENTEREMPLOYEES] = @original_WORKCENTEREMPLOYEES) OR ([WORKCENTEREMPLOYEES] IS NULL AND @original_WORKCENTEREMPLOYEES IS NULL)) AND (([OPERATIONSIGNAL] = @original_OPERATIONSIGNAL) OR ([OPERATIONSIGNAL] IS NULL AND @original_OPERATIONSIGNAL IS NULL)) AND (([TOOLS] = @original_TOOLS) OR ([TOOLS] IS NULL AND @original_TOOLS IS NULL)) AND (([PLANNEDWORKORDER] = @original_PLANNEDWORKORDER) OR ([PLANNEDWORKORDER] IS NULL AND @original_PLANNEDWORKORDER IS NULL)) AND (([SPACE1] = @original_SPACE1) OR ([SPACE1] IS NULL AND @original_SPACE1 IS NULL)) AND (([RME] = @original_RME) OR ([RME] IS NULL AND @original_RME IS NULL)) AND (([MAXQNT] = @original_MAXQNT) OR ([MAXQNT] IS NULL AND @original_MAXQNT IS NULL)) AND (([OEE] = @original_OEE) OR ([OEE] IS NULL AND @original_OEE IS NULL)) AND (([TOOLSCNT] = @original_TOOLSCNT) OR ([TOOLSCNT] IS NULL AND @original_TOOLSCNT IS NULL)) AND (([WORKTIME] = @original_WORKTIME) OR ([WORKTIME] IS NULL AND @original_WORKTIME IS NULL)) AND (([OEEINC] = @original_OEEINC) OR ([OEEINC] IS NULL AND @original_OEEINC IS NULL)) AND (([EMPLOYEESCONTROL] = @original_EMPLOYEESCONTROL) OR ([EMPLOYEESCONTROL] IS NULL AND @original_EMPLOYEESCONTROL IS NULL)) AND (([WORKGROUPSCONTROL] = @original_WORKGROUPSCONTROL) OR ([WORKGROUPSCONTROL] IS NULL AND @original_WORKGROUPSCONTROL IS NULL)) AND (([PRINTBARCODE] = @original_PRINTBARCODE) OR ([PRINTBARCODE] IS NULL AND @original_PRINTBARCODE IS NULL)) AND (([WCFREEQUANTITY] = @original_WCFREEQUANTITY) OR ([WCFREEQUANTITY] IS NULL AND @original_WCFREEQUANTITY IS NULL)) AND (([WCWTYPE] = @original_WCWTYPE) OR ([WCWTYPE] IS NULL AND @original_WCWTYPE IS NULL)) AND (([COMPLATEDQCONTROL] = @original_COMPLATEDQCONTROL) OR ([COMPLATEDQCONTROL] IS NULL AND @original_COMPLATEDQCONTROL IS NULL)) AND (([FCACTIVE] = @original_FCACTIVE) OR ([FCACTIVE] IS NULL AND @original_FCACTIVE IS NULL)) AND (([WODCTYPE] = @original_WODCTYPE) OR ([WODCTYPE] IS NULL AND @original_WODCTYPE IS NULL)) AND (([MAINWORKCENTER] = @original_MAINWORKCENTER) OR ([MAINWORKCENTER] IS NULL AND @original_MAINWORKCENTER IS NULL)) AND (([MAINWORKCENTERCODE] = @original_MAINWORKCENTERCODE) OR ([MAINWORKCENTERCODE] IS NULL AND @original_MAINWORKCENTERCODE IS NULL)) AND (([WORKCENTERTOOLS] = @original_WORKCENTERTOOLS) OR ([WORKCENTERTOOLS] IS NULL AND @original_WORKCENTERTOOLS IS NULL))" InsertCommand="INSERT INTO [WORKCENTERS] ([WORKGROUPID], [CODE], [NAME], [POLYVALENCE], [QUALITY], [DOWNTIME], [PERFORMANCE], [SETUP], [ACTIVE], [PERFMEASTIME], [ERRORTIME], [WAITTIME], [AVAILMEASTIME], [COST], [VIRTUAL], [WODC], [BACKTIME], [FORWARDTIME], [MESSAGE], [MESSAGENOTE], [SKIPDAY], [TERMINATEDOWNTIME], [WORKCENTEREMPLOYEES], [OPERATIONSIGNAL], [TOOLS], [PLANNEDWORKORDER], [SPACE1], [RME], [MAXQNT], [OEE], [TOOLSCNT], [WORKTIME], [OEEINC], [EMPLOYEESCONTROL], [WORKGROUPSCONTROL], [PRINTBARCODE], [WCFREEQUANTITY], [WCWTYPE], [COMPLATEDQCONTROL], [FCACTIVE], [WODCTYPE], [MAINWORKCENTER], [MAINWORKCENTERCODE], [WORKCENTERTOOLS]) VALUES (@WORKGROUPID, @CODE, @NAME, @POLYVALENCE, @QUALITY, @DOWNTIME, @PERFORMANCE, @SETUP, @ACTIVE, @PERFMEASTIME, @ERRORTIME, @WAITTIME, @AVAILMEASTIME, @COST, @VIRTUAL, @WODC, @BACKTIME, @FORWARDTIME, @MESSAGE, @MESSAGENOTE, @SKIPDAY, @TERMINATEDOWNTIME, @WORKCENTEREMPLOYEES, @OPERATIONSIGNAL, @TOOLS, @PLANNEDWORKORDER, @SPACE1, @RME, @MAXQNT, @OEE, @TOOLSCNT, @WORKTIME, @OEEINC, @EMPLOYEESCONTROL, @WORKGROUPSCONTROL, @PRINTBARCODE, @WCFREEQUANTITY, @WCWTYPE, @COMPLATEDQCONTROL, @FCACTIVE, @WODCTYPE, @MAINWORKCENTER, @MAINWORKCENTERCODE, @WORKCENTERTOOLS)" ConflictDetection="CompareAllValues" OldValuesParameterFormatString="original_{0}">
                    <DeleteParameters>
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_WORKGROUPID" Type="Int32" />
                        <asp:Parameter Name="original_CODE" Type="String" />
                        <asp:Parameter Name="original_NAME" Type="String" />
                        <asp:Parameter Name="original_POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="original_QUALITY" Type="Double" />
                        <asp:Parameter Name="original_DOWNTIME" Type="Double" />
                        <asp:Parameter Name="original_PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="original_SETUP" Type="Double" />
                        <asp:Parameter Name="original_ACTIVE" Type="Boolean" />
                        <asp:Parameter Name="original_PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ERRORTIME" Type="Double" />
                        <asp:Parameter Name="original_WAITTIME" Type="Double" />
                        <asp:Parameter Name="original_AVAILMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_COST" Type="Double" />
                        <asp:Parameter Name="original_VIRTUAL" Type="Boolean" />
                        <asp:Parameter Name="original_WODC" Type="Boolean" />
                        <asp:Parameter Name="original_BACKTIME" Type="Double" />
                        <asp:Parameter Name="original_FORWARDTIME" Type="Double" />
                        <asp:Parameter Name="original_MESSAGE" Type="Boolean" />
                        <asp:Parameter Name="original_MESSAGENOTE" Type="String" />
                        <asp:Parameter Name="original_SKIPDAY" Type="Byte" />
                        <asp:Parameter Name="original_TERMINATEDOWNTIME" Type="Boolean" />
                        <asp:Parameter Name="original_WORKCENTEREMPLOYEES" Type="Boolean" />
                        <asp:Parameter Name="original_OPERATIONSIGNAL" Type="Boolean" />
                        <asp:Parameter Name="original_TOOLS" Type="Boolean" />
                        <asp:Parameter Name="original_PLANNEDWORKORDER" Type="Boolean" />
                        <asp:Parameter Name="original_SPACE1" Type="String" />
                        <asp:Parameter Name="original_RME" Type="Boolean" />
                        <asp:Parameter Name="original_MAXQNT" Type="Double" />
                        <asp:Parameter Name="original_OEE" Type="Double" />
                        <asp:Parameter Name="original_TOOLSCNT" Type="Boolean" />
                        <asp:Parameter Name="original_WORKTIME" Type="Int32" />
                        <asp:Parameter Name="original_OEEINC" Type="Boolean" />
                        <asp:Parameter Name="original_EMPLOYEESCONTROL" Type="Boolean" />
                        <asp:Parameter Name="original_WORKGROUPSCONTROL" Type="Boolean" />
                        <asp:Parameter Name="original_PRINTBARCODE" Type="Boolean" />
                        <asp:Parameter Name="original_WCFREEQUANTITY" Type="Boolean" />
                        <asp:Parameter Name="original_WCWTYPE" Type="Int32" />
                        <asp:Parameter Name="original_COMPLATEDQCONTROL" Type="Boolean" />
                        <asp:Parameter Name="original_FCACTIVE" Type="Boolean" />
                        <asp:Parameter Name="original_WODCTYPE" Type="Byte" />
                        <asp:Parameter Name="original_MAINWORKCENTER" Type="Boolean" />
                        <asp:Parameter Name="original_MAINWORKCENTERCODE" Type="String" />
                        <asp:Parameter Name="original_WORKCENTERTOOLS" Type="Boolean" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="WORKGROUPID" Type="Int32" />
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="ACTIVE" Type="Boolean" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                        <asp:Parameter Name="COST" Type="Double" />
                        <asp:Parameter Name="VIRTUAL" Type="Boolean" />
                        <asp:Parameter Name="WODC" Type="Boolean" />
                        <asp:Parameter Name="BACKTIME" Type="Double" />
                        <asp:Parameter Name="FORWARDTIME" Type="Double" />
                        <asp:Parameter Name="MESSAGE" Type="Boolean" />
                        <asp:Parameter Name="MESSAGENOTE" Type="String" />
                        <asp:Parameter Name="SKIPDAY" Type="Byte" />
                        <asp:Parameter Name="TERMINATEDOWNTIME" Type="Boolean" />
                        <asp:Parameter Name="WORKCENTEREMPLOYEES" Type="Boolean" />
                        <asp:Parameter Name="OPERATIONSIGNAL" Type="Boolean" />
                        <asp:Parameter Name="TOOLS" Type="Boolean" />
                        <asp:Parameter Name="PLANNEDWORKORDER" Type="Boolean" />
                        <asp:Parameter Name="SPACE1" Type="String" />
                        <asp:Parameter Name="RME" Type="Boolean" />
                        <asp:Parameter Name="MAXQNT" Type="Double" />
                        <asp:Parameter Name="OEE" Type="Double" />
                        <asp:Parameter Name="TOOLSCNT" Type="Boolean" />
                        <asp:Parameter Name="WORKTIME" Type="Int32" />
                        <asp:Parameter Name="OEEINC" Type="Boolean" />
                        <asp:Parameter Name="EMPLOYEESCONTROL" Type="Boolean" />
                        <asp:Parameter Name="WORKGROUPSCONTROL" Type="Boolean" />
                        <asp:Parameter Name="PRINTBARCODE" Type="Boolean" />
                        <asp:Parameter Name="WCFREEQUANTITY" Type="Boolean" />
                        <asp:Parameter Name="WCWTYPE" Type="Int32" />
                        <asp:Parameter Name="COMPLATEDQCONTROL" Type="Boolean" />
                        <asp:Parameter Name="FCACTIVE" Type="Boolean" />
                        <asp:Parameter Name="WODCTYPE" Type="Byte" />
                        <asp:Parameter Name="MAINWORKCENTER" Type="Boolean" />
                        <asp:Parameter Name="MAINWORKCENTERCODE" Type="String" />
                        <asp:Parameter Name="WORKCENTERTOOLS" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="WORKGROUPID" Type="Int32" />
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="ACTIVE" Type="Boolean" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                        <asp:Parameter Name="COST" Type="Double" />
                        <asp:Parameter Name="VIRTUAL" Type="Boolean" />
                        <asp:Parameter Name="WODC" Type="Boolean" />
                        <asp:Parameter Name="BACKTIME" Type="Double" />
                        <asp:Parameter Name="FORWARDTIME" Type="Double" />
                        <asp:Parameter Name="MESSAGE" Type="Boolean" />
                        <asp:Parameter Name="MESSAGENOTE" Type="String" />
                        <asp:Parameter Name="SKIPDAY" Type="Byte" />
                        <asp:Parameter Name="TERMINATEDOWNTIME" Type="Boolean" />
                        <asp:Parameter Name="WORKCENTEREMPLOYEES" Type="Boolean" />
                        <asp:Parameter Name="OPERATIONSIGNAL" Type="Boolean" />
                        <asp:Parameter Name="TOOLS" Type="Boolean" />
                        <asp:Parameter Name="PLANNEDWORKORDER" Type="Boolean" />
                        <asp:Parameter Name="SPACE1" Type="String" />
                        <asp:Parameter Name="RME" Type="Boolean" />
                        <asp:Parameter Name="MAXQNT" Type="Double" />
                        <asp:Parameter Name="OEE" Type="Double" />
                        <asp:Parameter Name="TOOLSCNT" Type="Boolean" />
                        <asp:Parameter Name="WORKTIME" Type="Int32" />
                        <asp:Parameter Name="OEEINC" Type="Boolean" />
                        <asp:Parameter Name="EMPLOYEESCONTROL" Type="Boolean" />
                        <asp:Parameter Name="WORKGROUPSCONTROL" Type="Boolean" />
                        <asp:Parameter Name="PRINTBARCODE" Type="Boolean" />
                        <asp:Parameter Name="WCFREEQUANTITY" Type="Boolean" />
                        <asp:Parameter Name="WCWTYPE" Type="Int32" />
                        <asp:Parameter Name="COMPLATEDQCONTROL" Type="Boolean" />
                        <asp:Parameter Name="FCACTIVE" Type="Boolean" />
                        <asp:Parameter Name="WODCTYPE" Type="Byte" />
                        <asp:Parameter Name="MAINWORKCENTER" Type="Boolean" />
                        <asp:Parameter Name="MAINWORKCENTERCODE" Type="String" />
                        <asp:Parameter Name="WORKCENTERTOOLS" Type="Boolean" />
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_WORKGROUPID" Type="Int32" />
                        <asp:Parameter Name="original_CODE" Type="String" />
                        <asp:Parameter Name="original_NAME" Type="String" />
                        <asp:Parameter Name="original_POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="original_QUALITY" Type="Double" />
                        <asp:Parameter Name="original_DOWNTIME" Type="Double" />
                        <asp:Parameter Name="original_PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="original_SETUP" Type="Double" />
                        <asp:Parameter Name="original_ACTIVE" Type="Boolean" />
                        <asp:Parameter Name="original_PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ERRORTIME" Type="Double" />
                        <asp:Parameter Name="original_WAITTIME" Type="Double" />
                        <asp:Parameter Name="original_AVAILMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_COST" Type="Double" />
                        <asp:Parameter Name="original_VIRTUAL" Type="Boolean" />
                        <asp:Parameter Name="original_WODC" Type="Boolean" />
                        <asp:Parameter Name="original_BACKTIME" Type="Double" />
                        <asp:Parameter Name="original_FORWARDTIME" Type="Double" />
                        <asp:Parameter Name="original_MESSAGE" Type="Boolean" />
                        <asp:Parameter Name="original_MESSAGENOTE" Type="String" />
                        <asp:Parameter Name="original_SKIPDAY" Type="Byte" />
                        <asp:Parameter Name="original_TERMINATEDOWNTIME" Type="Boolean" />
                        <asp:Parameter Name="original_WORKCENTEREMPLOYEES" Type="Boolean" />
                        <asp:Parameter Name="original_OPERATIONSIGNAL" Type="Boolean" />
                        <asp:Parameter Name="original_TOOLS" Type="Boolean" />
                        <asp:Parameter Name="original_PLANNEDWORKORDER" Type="Boolean" />
                        <asp:Parameter Name="original_SPACE1" Type="String" />
                        <asp:Parameter Name="original_RME" Type="Boolean" />
                        <asp:Parameter Name="original_MAXQNT" Type="Double" />
                        <asp:Parameter Name="original_OEE" Type="Double" />
                        <asp:Parameter Name="original_TOOLSCNT" Type="Boolean" />
                        <asp:Parameter Name="original_WORKTIME" Type="Int32" />
                        <asp:Parameter Name="original_OEEINC" Type="Boolean" />
                        <asp:Parameter Name="original_EMPLOYEESCONTROL" Type="Boolean" />
                        <asp:Parameter Name="original_WORKGROUPSCONTROL" Type="Boolean" />
                        <asp:Parameter Name="original_PRINTBARCODE" Type="Boolean" />
                        <asp:Parameter Name="original_WCFREEQUANTITY" Type="Boolean" />
                        <asp:Parameter Name="original_WCWTYPE" Type="Int32" />
                        <asp:Parameter Name="original_COMPLATEDQCONTROL" Type="Boolean" />
                        <asp:Parameter Name="original_FCACTIVE" Type="Boolean" />
                        <asp:Parameter Name="original_WODCTYPE" Type="Byte" />
                        <asp:Parameter Name="original_MAINWORKCENTER" Type="Boolean" />
                        <asp:Parameter Name="original_MAINWORKCENTERCODE" Type="String" />
                        <asp:Parameter Name="original_WORKCENTERTOOLS" Type="Boolean" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="GroupsInWorkCenters" runat="server"
                    ConnectionString="<%$ ConnectionStrings:PMIDBWConnectionString2 %>"
                    SelectCommand="SELECT ID, CODE + ' ' + NAME AS CODE_NAME FROM WORKCENTERGROUPS"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
