﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Sections.aspx.cs" Inherits="ProjesisWeb.Definitions.Sections" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <%-- <div class="pager" role="group" aria-label="Basic example">
            <div class="btn-group" role="group">
                <dx:ASPxButton Text="Ekle" runat="server" ID="SectionsAddButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Sil" ID="SectionsDelButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Düzelt" ID="SectionsEdtButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Kaydet" ID="SectionsSavButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Vazgeç" ID="SectionsCanclButton"></dx:ASPxButton>
            </div>
        </div>--%>
        <hr style="border: groove" />

        <%--<div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Firma" ID="SectionsFirmLabel"></dx:ASPxLabel>
                <dx:ASPxComboBox runat="server" ID="SectionFirmComboBox" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith">
                    <Columns>
                        <dx:ListBoxColumn />
                        <dx:ListBoxColumn />
                        <dx:ListBoxColumn />
                    </Columns>
                </dx:ASPxComboBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Performans Ölçüm Süresi(dk)" ID="SectionsPerfTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsPerfTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kod" ID="SectionsCodeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsCodeTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kullanılabilirlik Ölçüm Süresi(dk)" ID="SectionsAvailTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsAvailTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Ad" ID="SectionNameLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsNameTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="İlk bekleme Süresi" ID="SectionsFirstWaitTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsFirstWaitTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kullanılabilirlik Hedefi %" ID="SectionsAvailTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsAvailTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Belirsiz Duruşa Geçme Süresi" ID="SectionsUnCertainStandLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsUnCertainStandTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Performans Hedefi %" ID="SectionsPermTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsPermTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Polivalans Kısıtı Uygulanacak mı?" ID="SectionsPoliConstLabel"></dx:ASPxLabel>
                <dx:ASPxCheckBox ID="SectionPoliConstComboBox" ClientInstanceName="DefaultCheckBox" runat="server" Checked="true">
                    <ClientSideEvents ValueChanged="updateCheckBoxState" Init="updateCheckBoxState" />
                </dx:ASPxCheckBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 form-group">
                <dx:ASPxLabel runat="server" Text="Kalite Hedefi %" ID="SectionsQualityTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsQualityTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-3 form-group">
                <dx:ASPxLabel runat="server" Text="OEE Hedefi %" ID="SectionsOEETargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsOEETargetTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Setup Hedefi/Değeri" ID="SectionsSetupTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="SectionsSetupTargetTextBox"></dx:ASPxTextBox>
            </div>
        </div>--%>
        <%--<hr style="border: groove" />--%>

        <div class="row">
            <div class="col-sm-12 form-group">
                <dx:ASPxGridView ID="SectionsGridView" runat="server" AutoGenerateColumns="False"
                    DataSourceID="SqlDataSource1" KeyFieldName="ID"
                    PageSize="10" AllowPaging="true" Width="100%" Theme="DevEx">
                    <Toolbars>
                        <dx:GridViewToolbar EnableAdaptivity="true">
                            <Items>
                                <dx:GridViewToolbarItem Text="Excel Aktar" Command="ExportToXlsx" />
                            </Items>
                        </dx:GridViewToolbar>
                    </Toolbars>
                    <SettingsAdaptivity>
                        <AdaptiveDetailLayoutProperties ColCount="1"></AdaptiveDetailLayoutProperties>
                    </SettingsAdaptivity>

                    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                    <SettingsSearchPanel Visible="True" />

                    <EditFormLayoutProperties ColCount="1"></EditFormLayoutProperties>
                    <Columns>
                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" VisibleIndex="1" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CODE" VisibleIndex="3" Caption="Kod">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ad" FieldName="NAME" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="POLYVALENCE" VisibleIndex="5" Caption="Polivalans">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="QUALITY" VisibleIndex="6" Caption="Kalite">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="DOWNTIME" VisibleIndex="7" Caption="Kull. Hedefi %">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFORMANCE" VisibleIndex="8" Caption="Perf. Hedefi %">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SETUP" VisibleIndex="9" Caption="Setep Hedefi">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFMEASTIME" VisibleIndex="10" Caption="Perf. Ölç. Sür.">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ERRORTIME" VisibleIndex="11">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="WAITTIME" VisibleIndex="12">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="AVAILMEASTIME" VisibleIndex="13" Caption="Kull. Ölç. Sür.">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Firma" FieldName="FIRMID" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="FirmInSections" TextField="PROJE" ValueField="ID">
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                    </Columns>
                    <Settings VerticalScrollableHeight="300" ShowFilterRow="True" ShowGroupPanel="True" ShowFooter="True" />
                    <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="WYSIWYG" />

                    <SettingsPager PageSize="5">
                        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                    </SettingsPager>
                    <SettingsBehavior AllowFocusedRow="True" ConfirmDelete="True" />
                    <SettingsCommandButton>
                        <ClearFilterButton Text="Temizle">
                        </ClearFilterButton>
                        <NewButton Text="Ekle"
                            RenderMode="Button"
                            Styles-Style-BackColor="Green">
                        </NewButton>
                        <UpdateButton Text="Güncelle"
                            RenderMode="Button">
                        </UpdateButton>
                        <CancelButton Text="Vazgeç"
                            RenderMode="Button">
                        </CancelButton>
                        <EditButton Text="Düzenle"
                            RenderMode="Button">
                        </EditButton>
                        <DeleteButton Text="Sil"
                            RenderMode="Button">
                        </DeleteButton>
                    </SettingsCommandButton>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                    ConnectionString="<%$ ConnectionStrings:PMIDBWConnectionString2 %>"
                    SelectCommand="SELECT * FROM [SECTIONS]"
                    DeleteCommand="DELETE FROM [SECTIONS] WHERE [ID] = @original_ID AND [FIRMID] = @original_FIRMID AND [CODE] = @original_CODE AND (([NAME] = @original_NAME) OR ([NAME] IS NULL AND @original_NAME IS NULL)) AND (([POLYVALENCE] = @original_POLYVALENCE) OR ([POLYVALENCE] IS NULL AND @original_POLYVALENCE IS NULL)) AND (([QUALITY] = @original_QUALITY) OR ([QUALITY] IS NULL AND @original_QUALITY IS NULL)) AND (([DOWNTIME] = @original_DOWNTIME) OR ([DOWNTIME] IS NULL AND @original_DOWNTIME IS NULL)) AND (([PERFORMANCE] = @original_PERFORMANCE) OR ([PERFORMANCE] IS NULL AND @original_PERFORMANCE IS NULL)) AND (([SETUP] = @original_SETUP) OR ([SETUP] IS NULL AND @original_SETUP IS NULL)) AND (([PERFMEASTIME] = @original_PERFMEASTIME) OR ([PERFMEASTIME] IS NULL AND @original_PERFMEASTIME IS NULL)) AND (([ERRORTIME] = @original_ERRORTIME) OR ([ERRORTIME] IS NULL AND @original_ERRORTIME IS NULL)) AND (([WAITTIME] = @original_WAITTIME) OR ([WAITTIME] IS NULL AND @original_WAITTIME IS NULL)) AND (([AVAILMEASTIME] = @original_AVAILMEASTIME) OR ([AVAILMEASTIME] IS NULL AND @original_AVAILMEASTIME IS NULL))"
                    InsertCommand="INSERT INTO [SECTIONS] ([FIRMID], [CODE], [NAME], [POLYVALENCE], [QUALITY], [DOWNTIME], [PERFORMANCE], [SETUP], [PERFMEASTIME], [ERRORTIME], [WAITTIME], [AVAILMEASTIME]) VALUES (@FIRMID, @CODE, @NAME, @POLYVALENCE, @QUALITY, @DOWNTIME, @PERFORMANCE, @SETUP, @PERFMEASTIME, @ERRORTIME, @WAITTIME, @AVAILMEASTIME)"
                    UpdateCommand="UPDATE [SECTIONS] SET [FIRMID] = @FIRMID, [CODE] = @CODE, [NAME] = @NAME, [POLYVALENCE] = @POLYVALENCE, [QUALITY] = @QUALITY, [DOWNTIME] = @DOWNTIME, [PERFORMANCE] = @PERFORMANCE, [SETUP] = @SETUP, [PERFMEASTIME] = @PERFMEASTIME, [ERRORTIME] = @ERRORTIME, [WAITTIME] = @WAITTIME, [AVAILMEASTIME] = @AVAILMEASTIME WHERE [ID] = @original_ID AND [FIRMID] = @original_FIRMID AND [CODE] = @original_CODE AND (([NAME] = @original_NAME) OR ([NAME] IS NULL AND @original_NAME IS NULL)) AND (([POLYVALENCE] = @original_POLYVALENCE) OR ([POLYVALENCE] IS NULL AND @original_POLYVALENCE IS NULL)) AND (([QUALITY] = @original_QUALITY) OR ([QUALITY] IS NULL AND @original_QUALITY IS NULL)) AND (([DOWNTIME] = @original_DOWNTIME) OR ([DOWNTIME] IS NULL AND @original_DOWNTIME IS NULL)) AND (([PERFORMANCE] = @original_PERFORMANCE) OR ([PERFORMANCE] IS NULL AND @original_PERFORMANCE IS NULL)) AND (([SETUP] = @original_SETUP) OR ([SETUP] IS NULL AND @original_SETUP IS NULL)) AND (([PERFMEASTIME] = @original_PERFMEASTIME) OR ([PERFMEASTIME] IS NULL AND @original_PERFMEASTIME IS NULL)) AND (([ERRORTIME] = @original_ERRORTIME) OR ([ERRORTIME] IS NULL AND @original_ERRORTIME IS NULL)) AND (([WAITTIME] = @original_WAITTIME) OR ([WAITTIME] IS NULL AND @original_WAITTIME IS NULL)) AND (([AVAILMEASTIME] = @original_AVAILMEASTIME) OR ([AVAILMEASTIME] IS NULL AND @original_AVAILMEASTIME IS NULL))" ConflictDetection="CompareAllValues" OldValuesParameterFormatString="original_{0}">
                    <DeleteParameters>
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_FIRMID" Type="Int32" />
                        <asp:Parameter Name="original_CODE" Type="String" />
                        <asp:Parameter Name="original_NAME" Type="String" />
                        <asp:Parameter Name="original_POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="original_QUALITY" Type="Double" />
                        <asp:Parameter Name="original_DOWNTIME" Type="Double" />
                        <asp:Parameter Name="original_PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="original_SETUP" Type="Double" />
                        <asp:Parameter Name="original_PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ERRORTIME" Type="Double" />
                        <asp:Parameter Name="original_WAITTIME" Type="Double" />
                        <asp:Parameter Name="original_AVAILMEASTIME" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="FIRMID" Type="Int32" />
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="FIRMID" Type="Int32" />
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_FIRMID" Type="Int32" />
                        <asp:Parameter Name="original_CODE" Type="String" />
                        <asp:Parameter Name="original_NAME" Type="String" />
                        <asp:Parameter Name="original_POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="original_QUALITY" Type="Double" />
                        <asp:Parameter Name="original_DOWNTIME" Type="Double" />
                        <asp:Parameter Name="original_PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="original_SETUP" Type="Double" />
                        <asp:Parameter Name="original_PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ERRORTIME" Type="Double" />
                        <asp:Parameter Name="original_WAITTIME" Type="Double" />
                        <asp:Parameter Name="original_AVAILMEASTIME" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <br />
                <asp:SqlDataSource ID="FirmInSections" runat="server" ConnectionString="<%$ ConnectionStrings:PMIDBWConnectionString2 %>"
                    SelectCommand="SELECT ID, CODE + ' ' + NAME AS PROJE FROM FIRM"></asp:SqlDataSource>
            </div>
        </div>

    </div>
</asp:Content>
