﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkCenterGroups.aspx.cs" Inherits="ProjesisWeb.Definitions.WorkCenterGroups" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <%--<div class="pager" role="group" aria-label="Basic example">
            <div class="btn-group" role="group">
                <dx:ASPxButton Text="Ekle" runat="server" ID="WCGroupsAddButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Sil" ID="WCGroupsDelButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Düzelt" ID="WCGroupsEdtButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Kaydet" ID="WCGroupsSavButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Vazgeç" ID="SWCGroupsCanclButton"></dx:ASPxButton>
            </div>
        </div>--%>


        <hr style="border: groove" />
        <%--<b style="margin-left: 10px;">Detay</b>--%>


        <%--        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Bölüm" ID="WCGroupsSectionLabel"></dx:ASPxLabel>
                <dx:ASPxComboBox runat="server" ID="WCGroupsSectionComboBox" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith">
                    <Columns>
                        <dx:ListBoxColumn />
                        <dx:ListBoxColumn />
                        <dx:ListBoxColumn />
                    </Columns>
                </dx:ASPxComboBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Performans Ölçüm Süresi(dk)" ID="WCGroupsPerfTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsPerfTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kod" ID="WCGroupsCodeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsCodeTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kullanılabilirlik Ölçüm Süresi(dk)" ID="WCGroupsAvailTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsAvailTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Ad" ID="WCGroupsNameLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsNameTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="İlk bekleme Süresi" ID="WCGroupsFirstWaitTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsFirstWaitTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kullanılabilirlik Hedefi %" ID="WCGroupsAvailTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsAvailTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Belirsiz Duruşa Geçme Süresi" ID="WCGroupsUnCertainStandLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsUnCertainStandTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Performans Hedefi %" ID="WCGroupsPermTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsPermTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Polivalans Kısıtı Uygulanacak mı?" ID="WCGroupsPoliConstLabel"></dx:ASPxLabel>
                <dx:ASPxCheckBox ID="WCGroupsPoliConstCheckBox" ClientInstanceName="DefaultCheckBox" runat="server" Checked="true">
                    <ClientSideEvents ValueChanged="updateCheckBoxState" Init="updateCheckBoxState" />
                </dx:ASPxCheckBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 form-group">
                <dx:ASPxLabel runat="server" Text="Kalite Hedefi %" ID="WCGroupsQualityTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsQualityTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-3 form-group">
                <dx:ASPxLabel runat="server" Text="OEE Hedefi %" ID="WCGroupsOEETargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupsOEETargetTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Setup Hedefi/Değeri" ID="WCGroupsSetupTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="WCGroupskSetupTargetTextBox"></dx:ASPxTextBox>
            </div>
        </div>--%>

        <div class="row">
            <div class="col-sm-12 form-group">
                <dx:ASPxGridView ID="WCGroupsGridView" runat="server" AutoGenerateColumns="False"
                    DataSourceID="SqlDataSource2" KeyFieldName="ID"
                    PageSize="10" AllowPaging="true" Width="100%" Theme="DevEx">
                    <Settings VerticalScrollableHeight="300" ShowFilterRow="True" ShowGroupPanel="True" ShowFooter="True" />
                    <SettingsAdaptivity>
                        <AdaptiveDetailLayoutProperties ColCount="1"></AdaptiveDetailLayoutProperties>
                    </SettingsAdaptivity>

                    <SettingsPager PageSize="5">
                        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                    </SettingsPager>

                    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                    <SettingsBehavior AllowFocusedRow="True" ConfirmDelete="True" />
                    <SettingsSearchPanel Visible="True" />

                    <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="WYSIWYG" />

                    <EditFormLayoutProperties ColCount="1"></EditFormLayoutProperties>
                    <Columns>
                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" VisibleIndex="1" Visible="False" ShowInCustomizationForm="True">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CODE" VisibleIndex="3" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="NAME" ShowInCustomizationForm="True" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="POLYVALENCE" VisibleIndex="5" ShowInCustomizationForm="True">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="QUALITY" VisibleIndex="6" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="DOWNTIME" VisibleIndex="7" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFORMANCE" VisibleIndex="8" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SETUP" VisibleIndex="9" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFMEASTIME" VisibleIndex="10" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ERRORTIME" VisibleIndex="11" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="WAITTIME" VisibleIndex="12" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="AVAILMEASTIME" VisibleIndex="13" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>

                        <%--<dx:GridViewDataComboBoxColumn Caption="Bölüm" FieldName="FIRMID" VisibleIndex="2">
                    <PropertiesComboBox DataSourceID="FirmInSections" TextField="CODE" ValueField="ID">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>--%>
                        <dx:GridViewDataComboBoxColumn FieldName="SECTIONID" ShowInCustomizationForm="True" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="SectionsInWorkCenterGroups" TextField="CODE_NAME" ValueField="ID">
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                    </Columns>
                    <SettingsCommandButton>
                        <NewButton Text="Ekle"
                            RenderMode="Button"
                            Styles-Style-BackColor="Green">
                        </NewButton>
                        <UpdateButton Text="Güncelle"
                            RenderMode="Button">
                        </UpdateButton>
                        <CancelButton Text="Vazgeç"
                            RenderMode="Button">
                        </CancelButton>
                        <EditButton Text="Düzenle"
                            RenderMode="Button">
                        </EditButton>
                        <DeleteButton Text="Sil"
                            RenderMode="Button">
                        </DeleteButton>
                        <ClearFilterButton Text="Temizle">
                        </ClearFilterButton>
                    </SettingsCommandButton>
                    <Toolbars>
                        <dx:GridViewToolbar EnableAdaptivity="true">
                            <Items>
                                <dx:GridViewToolbarItem Text="Excel Aktar" Command="ExportToXlsx" />
                            </Items>
                        </dx:GridViewToolbar>
                    </Toolbars>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                    ConnectionString="<%$ ConnectionStrings:PMIDBWConnectionString2 %>"
                    SelectCommand="SELECT [ID], [CODE], [SECTIONID], [NAME], [POLYVALENCE], [QUALITY], [DOWNTIME], [PERFORMANCE], [SETUP], [PERFMEASTIME], [ERRORTIME], [WAITTIME], [AVAILMEASTIME] FROM [WORKCENTERGROUPS]" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM WORKCENTERGROUPS WHERE (ID = @original_ID) AND (CODE = @original_CODE) AND (SECTIONID = @original_SECTIONID) AND (NAME = @original_NAME OR NAME IS NULL AND @original_NAME IS NULL) AND (POLYVALENCE = @original_POLYVALENCE OR POLYVALENCE IS NULL AND @original_POLYVALENCE IS NULL) AND (QUALITY = @original_QUALITY OR QUALITY IS NULL AND @original_QUALITY IS NULL) AND (DOWNTIME = @original_DOWNTIME OR DOWNTIME IS NULL AND @original_DOWNTIME IS NULL) AND (PERFORMANCE = @original_PERFORMANCE OR PERFORMANCE IS NULL AND @original_PERFORMANCE IS NULL) AND (SETUP = @original_SETUP OR SETUP IS NULL AND @original_SETUP IS NULL) AND (PERFMEASTIME = @original_PERFMEASTIME OR PERFMEASTIME IS NULL AND @original_PERFMEASTIME IS NULL) AND (ERRORTIME = @original_ERRORTIME OR ERRORTIME IS NULL AND @original_ERRORTIME IS NULL) AND (WAITTIME = @original_WAITTIME OR WAITTIME IS NULL AND @original_WAITTIME IS NULL) AND (AVAILMEASTIME = @original_AVAILMEASTIME OR AVAILMEASTIME IS NULL AND @original_AVAILMEASTIME IS NULL)" InsertCommand="INSERT INTO WORKCENTERGROUPS(SECTIONID, CODE, NAME, POLYVALENCE, QUALITY, DOWNTIME, PERFORMANCE, SETUP, PERFMEASTIME, ERRORTIME, WAITTIME, AVAILMEASTIME) VALUES (@SECTIONID, @CODE, @NAME, @POLYVALENCE, @QUALITY, @DOWNTIME, @PERFORMANCE, @SETUP, @PERFMEASTIME, @ERRORTIME, @WAITTIME, @AVAILMEASTIME)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE WORKCENTERGROUPS SET SECTIONID = @SECTIONID, CODE = @CODE, NAME = @NAME, POLYVALENCE = @POLYVALENCE, QUALITY = @QUALITY, DOWNTIME = @DOWNTIME, PERFORMANCE = @PERFORMANCE, SETUP = @SETUP, PERFMEASTIME = @PERFMEASTIME, ERRORTIME = @ERRORTIME, WAITTIME = @WAITTIME, AVAILMEASTIME = @AVAILMEASTIME WHERE (ID = @original_ID) AND (SECTIONID = @original_SECTIONID) AND (CODE = @original_CODE) AND (NAME = @original_NAME OR NAME IS NULL AND @original_NAME IS NULL) AND (POLYVALENCE = @original_POLYVALENCE OR POLYVALENCE IS NULL AND @original_POLYVALENCE IS NULL) AND (QUALITY = @original_QUALITY OR QUALITY IS NULL AND @original_QUALITY IS NULL) AND (DOWNTIME = @original_DOWNTIME OR DOWNTIME IS NULL AND @original_DOWNTIME IS NULL) AND (PERFORMANCE = @original_PERFORMANCE OR PERFORMANCE IS NULL AND @original_PERFORMANCE IS NULL) AND (SETUP = @original_SETUP OR SETUP IS NULL AND @original_SETUP IS NULL) AND (PERFMEASTIME = @original_PERFMEASTIME OR PERFMEASTIME IS NULL AND @original_PERFMEASTIME IS NULL) AND (ERRORTIME = @original_ERRORTIME OR ERRORTIME IS NULL AND @original_ERRORTIME IS NULL) AND (WAITTIME = @original_WAITTIME OR WAITTIME IS NULL AND @original_WAITTIME IS NULL) AND (AVAILMEASTIME = @original_AVAILMEASTIME OR AVAILMEASTIME IS NULL AND @original_AVAILMEASTIME IS NULL)">
                    <DeleteParameters>
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_CODE" Type="String" />
                        <asp:Parameter Name="original_SECTIONID" Type="Int32" />
                        <asp:Parameter Name="original_NAME" Type="String" />
                        <asp:Parameter Name="original_POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="original_QUALITY" Type="Double" />
                        <asp:Parameter Name="original_DOWNTIME" Type="Double" />
                        <asp:Parameter Name="original_PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="original_SETUP" Type="Double" />
                        <asp:Parameter Name="original_PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ERRORTIME" Type="Double" />
                        <asp:Parameter Name="original_WAITTIME" Type="Double" />
                        <asp:Parameter Name="original_AVAILMEASTIME" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="SECTIONID" Type="Int32" />
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="SECTIONID" Type="Int32" />
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_SECTIONID" Type="Int32" />
                        <asp:Parameter Name="original_CODE" />
                        <asp:Parameter Name="original_NAME" Type="String" />
                        <asp:Parameter Name="original_POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="original_QUALITY" Type="Double" />
                        <asp:Parameter Name="original_DOWNTIME" Type="Double" />
                        <asp:Parameter Name="original_PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="original_SETUP" Type="Double" />
                        <asp:Parameter Name="original_PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="original_ERRORTIME" Type="Double" />
                        <asp:Parameter Name="original_WAITTIME" Type="Double" />
                        <asp:Parameter Name="original_AVAILMEASTIME" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="SectionsInWorkCenterGroups" runat="server" ConnectionString="<%$ ConnectionStrings:PMIDBWConnectionString2 %>" SelectCommand="SELECT ID, CODE + ' ' + NAME AS CODE_NAME FROM SECTIONS"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
