﻿using DevExpress.Web;
using System;

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjesisWeb.Definitions
{
    public partial class Firms : Page
    {


        Methods.DefinitionMethods dm = new Methods.DefinitionMethods();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {              
                return;
            }
            //GridViewFeaturesHelper.SetupGlobalGridViewBehavior(FirmsGridView);
            //FirmsGridView.AddNewRow();
            //FirmsGridView.DataBind();
            //DisableUpdateInDemo();

            //dm.ListFirms(FirmsGridView);

        }


        //DataTable GetTable()
        //{
        //    //You can store a DataTable in the session state
        //    DataTable table = Session["Table"] as DataTable;
        //    if (table == null)
        //    {
        //        table = new DataTable();

        //        DataColumn idColumn = table.Columns.Add("ID", typeof(Int32));
        //        DataColumn codeColumn = table.Columns.Add("CODE", typeof(String));
        //        DataColumn nameColumn = table.Columns.Add("NAME", typeof(String));
        //        DataColumn polyColumn= table.Columns.Add("POLYVALENCE", typeof(Boolean));
        //        DataColumn qualityColumn = table.Columns.Add("QUALITY", typeof(float));
        //        DataColumn downtimeColumn = table.Columns.Add("DOWNTIME", typeof(float));
        //        DataColumn performansColumn = table.Columns.Add("PERFORMANCE", typeof(float));
        //        DataColumn setupColumn = table.Columns.Add("SETUP", typeof(float));
        //        DataColumn perfEasTimeColumn = table.Columns.Add("PERFMEASTIME", typeof(int));
        //        DataColumn errorTimeColumn = table.Columns.Add("ERRORTIME", typeof(float));
        //        DataColumn waitTimeColumn = table.Columns.Add("WAITTIME", typeof(float));
        //        DataColumn availMeasTimeColumn = table.Columns.Add("AVAILMEASTIME", typeof(int));


        //        table.PrimaryKey = new DataColumn[] { idColumn };
        //        idColumn.ReadOnly = true;

        //        for (int i = 1; i < 23; i++)
        //        {
        //            DataRow aRow = table.NewRow();
        //            //aRow["ID"] = i;
        //            aRow["CODE"] = String.Format("CODE{0}", i);
        //            aRow["NAME"] = String.Format("NAME{0}", i);
        //            aRow["POLYVALENCE"] = String.Format("POLYVALENCE{0}", i);
        //            aRow["QUALITY"] = String.Format("QUALITY{0}", i);
        //            aRow["DOWNTIME"] = String.Format("DOWNTIME{0}", i);
        //            aRow["PERFORMANCE"] = String.Format("PERFORMANCE{0}", i);
        //            aRow["SETUP"] = String.Format("SETUP{0}", i);
        //            aRow["PERFMEASTIME"] = String.Format("PERFMEASTIME{0}", i);
        //            aRow["ERRORTIME"] = String.Format("ERRORTIME{0}", i);
        //            aRow["WAITTIME"] = String.Format("WAITTIME{0}", i);
        //            aRow["AVAILMEASTIME"] = String.Format("AVAILMEASTIME{0}", i);

        //            table.Rows.Add(aRow);
        //        }
        //        Session["Table"] = table;
        //    }
        //    return table;
        //}

        //public void ListFirmGridView()
        //{
        //    DB.ConnectionDB cdb = new DB.ConnectionDB();
        //    cdb.ConnectDBMethod();
        //    SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM FIRM", cdb.conn);
        //    using (DataTable dt = new DataTable())
        //    {
        //        adapter.Fill(dt);
        //        FirmsGridView.DataSource = dt;
        //        FirmsGridView.DataBind();
        //    }
        //    cdb.Dispose(cdb.conn);
        //}

    }
}