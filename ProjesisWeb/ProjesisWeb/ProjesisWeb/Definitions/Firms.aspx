﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Firms.aspx.cs" Inherits="ProjesisWeb.Definitions.Firms" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%-- <div class="options">
        <div class="options-item">
            <dx:ASPxComboBox runat="server" ID="ddlEditMode" AutoPostBack="true" Width="200"
                OnSelectedIndexChanged="ddlEditMode_SelectedIndexChanged"  Caption="Edit Mode">
                <RootStyle CssClass="OptionsBottomMargin" />
            </dx:ASPxComboBox>
        </div>
    </div>--%>

    <%--<asp:Content ID="Content1" ContentPlaceHolderID="ControlOptionsTopHolder" runat="server">
    
</asp:Content>--%>



    <div class="container">
        <%--<div class="pager" role="group" aria-label="Basic example">
            <div class="btn-group" role="group">
                <dx:ASPxButton Text="Ekle" runat="server" ID="FirmsAddButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Sil" ID="FirmsDelButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Düzelt" ID="FirmsEdtButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Kaydet" ID="FirmsSavButton"></dx:ASPxButton>
            </div>
            <div class="btn-group" role="group">
                <dx:ASPxButton runat="server" Text="Vazgeç" ID="FirmsCanclButton"></dx:ASPxButton>
            </div>
        </div>--%>
        <hr style="border: groove" />
        <%--<b style="margin-left: 10px;">Detay</b>
        <hr style="border: groove" />--%>

        <%--        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kod" ID="FirmCodeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmsCodeTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Performans Ölçüm Süresi(dk)" ID="FirmPerfTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmsPerfTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Ad" ID="FirmNameLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmsNameTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kullanılabilirlik Ölçüm Süresi(dk)" ID="FirmAvailTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmAvailTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Kullanılabilirlik Hedefi %" ID="FirmAvailTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmAvailTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="İlk bekleme Süresi" ID="FirmFirstWaitTimeLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmFirstWaitTimeTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Performans Hedefi %" ID="FirmPermTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmPermTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Belirsiz Duruşa Geçme Süresi" ID="FirmUnCertainStandLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmUnCertainStandTextBox"></dx:ASPxTextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 form-group">
                <dx:ASPxLabel runat="server" Text="Kalite Hedefi %" ID="FirmQualityTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmQualityTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-3 form-group">
                <dx:ASPxLabel runat="server" Text="OEE Hedefi %" ID="FirmOEETargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmOEETargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-3 form-group">
                <dx:ASPxLabel runat="server" Text="Polivalans Kısıtı Uygulanacak mı?" ID="firmPoliConstLabel"></dx:ASPxLabel>
                <dx:ASPxCheckBox ID="FirmsPoliConstCheckBox" ClientInstanceName="DefaultCheckBox" runat="server" Checked="true">
                    <ClientSideEvents ValueChanged="updateCheckBoxState" Init="updateCheckBoxState" />
                </dx:ASPxCheckBox>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                <dx:ASPxLabel runat="server" Text="Setup Hedefi/Değeri" ID="FirmSetupTargetLabel"></dx:ASPxLabel>
                <dx:ASPxTextBox runat="server" ID="FirmSetupTargetTextBox"></dx:ASPxTextBox>
            </div>
            <div class="col-sm-6 form-group">
                <dx:ASPxButton runat="server" Text="Kaydet" ID="FirmsSavButton"></dx:ASPxButton>
                <dx:ASPxButton runat="server" Text="Vazgeç" ID="FirmsCanclButton"></dx:ASPxButton>
            </div>
        </div>

        <hr style="border: groove" />--%>

        <div class="row">
            <div class="col-sm-12 form-group">
                <dx:ASPxGridView ID="FirmsGridView" runat="server" KeyFieldName="ID" 
                    PageSize="2" AllowPaging="true" AutoGenerateColumns="False" Width="100%"
                    DataSourceID="FIRM" Theme="DevEx">

                    <Toolbars>
                        <dx:GridViewToolbar EnableAdaptivity="true">
                            <Items>
                                <dx:GridViewToolbarItem Text="Excel Aktar" Command="ExportToXlsx" />
                            </Items>
                        </dx:GridViewToolbar>
                    </Toolbars>

                    <SettingsSearchPanel Visible="true" />

                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" VisibleIndex="1" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CODE" VisibleIndex="2" Caption="Kod">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="POLYVALENCE" VisibleIndex="4" Caption="Polivalans">
                            <PropertiesCheckEdit DisplayTextChecked="Evet" DisplayTextUnchecked="Hayır">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="NAME" VisibleIndex="3" Caption="Ad">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="QUALITY" VisibleIndex="5" Caption="Kalite Hedefi %">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="DOWNTIME" VisibleIndex="6" Caption="Kull. Hedefi %">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFORMANCE" VisibleIndex="7" Caption="Perf. Hedefi %">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SETUP" VisibleIndex="8" Caption="Setup Hedefi">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PERFMEASTIME" VisibleIndex="9" Caption="Perf. Ölç. Sür.">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ERRORTIME" VisibleIndex="10">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="WAITTIME" VisibleIndex="11">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="AVAILMEASTIME" VisibleIndex="12" Caption="Kull. Ölç. Sür.">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewCommandColumn ShowApplyFilterButton="True"
                            ShowCancelButton="True" ShowClearFilterButton="True"
                            ShowDeleteButton="True" ShowEditButton="True"
                            ShowNewButtonInHeader="True" ShowRecoverButton="False"
                            ShowUpdateButton="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                    </Columns>

                    <Settings VerticalScrollableHeight="300" ShowFilterRow="True" ShowGroupPanel="True" ShowFooter="True" />
                    <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="WYSIWYG" />
                    <SettingsAdaptivity>
                        <AdaptiveDetailLayoutProperties ColCount="1"></AdaptiveDetailLayoutProperties>
                    </SettingsAdaptivity>

                    <SettingsPager PageSize="2">
                        <PageSizeItemSettings Items="10, 20, 50" Visible="true" ShowAllItem="true" />
                    </SettingsPager>
                    <EditFormLayoutProperties>
                        <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="700" />
                    </EditFormLayoutProperties>

                    <SettingsBehavior AllowFocusedRow="True" ConfirmDelete="True" />
                    <SettingsCommandButton >
                        <NewButton Text="Ekle"
                            RenderMode="Button"                          
                            Styles-Style-BackColor="Green">
                        </NewButton>
                        <UpdateButton Text="Güncelle" 
                            RenderMode="Button">
                        </UpdateButton>
                        <CancelButton Text="Vazgeç"
                            RenderMode="Button">
                        </CancelButton>
                        <EditButton Text="Düzenle" 
                           RenderMode="Button" 
                          >
                        </EditButton>
                        <DeleteButton Text="Sil"
                            RenderMode="Button">
                        </DeleteButton>
                        <ClearFilterButton Text="Temizle">
                        </ClearFilterButton>
                    </SettingsCommandButton>

                    <SettingsPopup>
                        <EditForm Width="600">
                            <SettingsAdaptivity Mode="OnWindowInnerWidth" SwitchAtWindowInnerWidth="768" />
                        </EditForm>
                    </SettingsPopup>
                </dx:ASPxGridView>

                <asp:SqlDataSource ID="FIRM" runat="server"
                    ConnectionString="<%$ ConnectionStrings:PMIDBWConnectionString2 %>" DeleteCommand="DELETE FROM [FIRM] WHERE [ID] = @ID"
                    InsertCommand="INSERT INTO [FIRM] ([CODE], [NAME], [POLYVALENCE], [QUALITY], [DOWNTIME], [PERFORMANCE], [SETUP], [PERFMEASTIME], [ERRORTIME], [WAITTIME], [AVAILMEASTIME]) VALUES (@CODE, @NAME, @POLYVALENCE, @QUALITY, @DOWNTIME, @PERFORMANCE, @SETUP, @PERFMEASTIME, @ERRORTIME, @WAITTIME, @AVAILMEASTIME)"
                    SelectCommand="SELECT * FROM [FIRM]"
                    UpdateCommand="UPDATE [FIRM] SET [CODE] = @CODE, [NAME] = @NAME, [POLYVALENCE] = @POLYVALENCE, [QUALITY] = @QUALITY, [DOWNTIME] = @DOWNTIME, [PERFORMANCE] = @PERFORMANCE, [SETUP] = @SETUP, [PERFMEASTIME] = @PERFMEASTIME, [ERRORTIME] = @ERRORTIME, [WAITTIME] = @WAITTIME, [AVAILMEASTIME] = @AVAILMEASTIME WHERE [ID] = @ID">
                    <DeleteParameters>
                        <asp:Parameter Name="ID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="CODE" Type="String" />
                        <asp:Parameter Name="NAME" Type="String" />
                        <asp:Parameter Name="POLYVALENCE" Type="Boolean" />
                        <asp:Parameter Name="QUALITY" Type="Double" />
                        <asp:Parameter Name="DOWNTIME" Type="Double" />
                        <asp:Parameter Name="PERFORMANCE" Type="Double" />
                        <asp:Parameter Name="SETUP" Type="Double" />
                        <asp:Parameter Name="PERFMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ERRORTIME" Type="Double" />
                        <asp:Parameter Name="WAITTIME" Type="Double" />
                        <asp:Parameter Name="AVAILMEASTIME" Type="Int32" />
                        <asp:Parameter Name="ID" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </div>
        </div>
    </div>

</asp:Content>
